Chromaprint JNI Wrapper for Android
===================================

NOT FINISHED, NOT TESTED

Either you need to clone this repository with all submodules:

    git clone --recursive https://bitbucket.org/lalinsky/chromaprint-android.git

Or you need to make sure you have the Chromaprint and FFT submodules downloaded:

    git clone https://bitbucket.org/lalinsky/chromaprint-android.git
    git submodule update --init --recursive

Build the JNI library:

    ndk-build

Build the Android project:

    android update project --path .
    ant release

Copy bin/classes.jar and libs/\*/libchromaprint-jni.so to your project.
